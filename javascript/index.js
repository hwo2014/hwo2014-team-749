var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

var PI = 3.14159265359;

var carColor;
var track;
var lastIndex = -1;
var lastPosition = -1;
var speed = -1;
var lastAngle = -1;
var angleData = {};
var forceData = [];
var lap = 0;
var F0 = 0;
var cd = 0;
var curve = {};
var allRadius = [];
var laneDist;
var Vc = -1;
var Aest = -1;
var changed = 0;
var Rc = -1;

insertRadius = function(r) {
  var x = 0;
  for (i in allRadius) {
    if (allRadius[i]==r) break;
    x++;
    if (x==allRadius.length) {
      allRadius.push(r);
      break;
    }
  }
  if (allRadius.length==0) allRadius.push(r);
}

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

var fs = require('fs');
var stream = fs.createWriteStream("data.txt");

jsonStream = client.pipe(JSONStream.parse());

send({"msgType": "joinRace", "data": { "botId": { "name": "Ghost", "key": "/DVYNIZIZcWKzg" }, "trackName": "germany", "carCount": 1 }});


jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {

    if (Vc!=-1 && !changed) {
      //send({"msgType": "switchLane", "data": "Right"});
      //laneDist = -laneDist;
      changed = 1;
    }

    var allData = data.data;

    for (var i in allData) {
      item = allData[i];
      if (item.id.color == carColor) {

        var pos = item.piecePosition;

        var index = pos.pieceIndex;

        var dist = 0.0;

        if (lastIndex!=-1) {
          if (index==lastIndex) {
            dist = pos.inPieceDistance - lastPosition;
            //console.dir(dist);
          }
          else {
            dist = 0;
            if (track[lastIndex].length) {
              dist += track[lastIndex].length - lastPosition;
            }
            else {
              var r = track[lastIndex].radius;
              if (track[lastIndex].angle<0.0) r -= laneDist;
              else r += laneDist;
              dist += r*Math.abs(track[lastIndex].angle)/180.0*PI - lastPosition;
            }

            dist += pos.inPieceDistance;

          }
        }

        lastIndex = index;
        lastPosition = pos.inPieceDistance;

        //console.dir("force");
        //console.dir(dist - speed);

        //stream.write(dist - speed + " " + dist + "\n");
        var lastSpeed = speed;
        speed = dist;
        //console.dir(pos.lap);
        if (pos.lap==0) {

          console.dir(speed);
          //console.dir(item.angle);
          if (!track[index].length) {
            //console.dir("data");
            //console.dir(track[index].radius);
            //console.dir(speed*speed/track[index].radius);

            

            stream.write(speed + " " + item.angle + "\n");
          }
          else {
            if (forceData.length<20) {
              if (lastSpeed>0.0) {
                forceData.push({"force": speed - lastSpeed, "v": speed});
              }
            }
          }

          if (track[index].length) {

            throttle = 1.0;
            if (forceData.length==20) {

              if (lap==0) {
                lap = 1;

                console.dir(Vc);
                console.dir(Aest);

                var sumf = 0;
                var sumv = 0;
                var sumvv = 0;

                for (i in forceData) {
                  sumf = sumf  + forceData[i].force;
                  sumv = sumv  + forceData[i].v;
                }

                var fm = 1.0*sumf / forceData.length;
                var vm = 1.0*sumv / forceData.length;

                sumf = 0;
                sumv = 0;
                sumvv = 0;

                for (i in forceData) {
                  sumf = sumf  + (forceData[i].force - fm)*(forceData[i].v - vm);
                  sumvv = sumvv  + (forceData[i].v - vm)*(forceData[i].v - vm);
                }

                cd = sumf / sumvv;

                F0 = fm - cd*vm;

              }

              if (Vc<0.0)
                throttle = 0.2;
              else {
                throttle = 0.6;

                var throttle;

                var totalLength = 1.0*track[index].length - 1.0*pos.inPieceDistance;

                var j = index + 1;

                while (true) {
                  j = j%track.length;

                  if (track[j].length) {
                    totalLength = totalLength + 1.0*track[j].length;
                    j = j+1;
                  }
                  else break;
                }

                var nextSpeed = speed + F0 + cd*speed;

                j = j%track.length;

                console.dir(totalLength);

                var _totalLength = totalLength;

                for (; totalLength>nextSpeed;) {
                  nextSpeed = nextSpeed + cd*nextSpeed;
                  totalLength = totalLength - nextSpeed;
                  if (nextSpeed<2.0) break;
                }

                var r = track[j].radius;
                if (track[j].angle<0.0) r -= laneDist;
                else r += laneDist;

                console.dir(nextSpeed);

                var throttle;

                if (nextSpeed*nextSpeed/r > (Math.sqrt(Vc*r)+0.5)*(Math.sqrt(Vc*r) + 0.5)/r) {
                  nextSpeed = speed + 0.5*F0 + cd*speed;

                  j = j%track.length;

                  totalLength = _totalLength;

                  for (; totalLength>nextSpeed;) {
                    nextSpeed = nextSpeed + cd*nextSpeed;
                    totalLength = totalLength - nextSpeed;
                    if (nextSpeed<2.0) break;
                  }

                  var r = track[j].radius;
                  if (track[j].angle<0.0) r -= laneDist;
                  else r += laneDist;

                  if (nextSpeed*nextSpeed/r > (Math.sqrt(Vc*r)+0.5)*(Math.sqrt(Vc*r) + 0.5)/r) throttle = 0.0;
                  else throttle = 0.5;
                }
                else throttle = 1.0;
              }
            }
            send({
              msgType: "throttle",
              data: throttle
            });
          }
          else {

            var r = track[index].radius;
            if (track[index].angle<0.0) r -= laneDist;
            else r += laneDist;

            if (Math.abs(item.angle) < 0.000001) throttle = 1.0;
            else {
              if (Vc==-1) {
                Vc = speed*speed/r;
                Rc = r;
              }
              if (speed*speed/r>(Math.sqrt(Vc*r)+0.2)*(Math.sqrt(Vc*r) + 0.2)/r) throttle = 0.0;
              else throttle = 1.0;

              if (Math.abs(speed*speed/r-Vc)<0.10) Aest = Math.max(Aest, item.angle);
            }

            console.dir("Velocidad");
            console.dir(speed);
            console.dir(throttle);

            console.dir(Math.sqrt(Vc*Rc)+0.2);
            console.dir(Aest);

            send({
              msgType: "throttle",
              data: throttle
            });
          }

        }
        else {

          if (track[index].length) {

            var throttle;

            var totalLength = 1.0*track[index].length - 1.0*pos.inPieceDistance;

            var j = index + 1;

            while (true) {
              j = j%track.length;

              if (track[j].length) {
                totalLength = totalLength + 1.0*track[j].length;
                j = j+1;
              }
              else break;
            }

            var nextSpeed = speed + F0 + cd*speed;

            j = j%track.length;

            console.dir(totalLength);

            var _totalLength = totalLength;

            for (; totalLength>nextSpeed;) {
              nextSpeed = nextSpeed + cd*nextSpeed;
              totalLength = totalLength - nextSpeed;
              if (nextSpeed<2.0) break;
            }

            var r = track[j].radius;
            if (track[j].angle<0.0) r -= laneDist;
            else r += laneDist;

            console.dir(nextSpeed);

            var throttle;

            if (nextSpeed > (Math.sqrt(Vc*r)+0.8)) {
              nextSpeed = speed + 0.5*F0 + cd*speed;

              j = j%track.length;

              totalLength = _totalLength;

              for (; totalLength>nextSpeed;) {
                nextSpeed = nextSpeed + cd*nextSpeed;
                totalLength = totalLength - nextSpeed;
                if (nextSpeed<2.0) break;
              }

              var r = track[j].radius;
              if (track[j].angle<0.0) r -= laneDist;
              else r += laneDist;

              if (nextSpeed > Vc + (Math.sqrt(Vc*r)+0.8)) throttle = 0.0;
              else throttle = 0.5;
            }
            else throttle = 1.0;

            console.dir(Vc + (Math.sqrt(Vc*r)+0.8));

            send({
              msgType: "throttle",
              data: throttle
            });
          }
          else {

            var r = track[index].radius;
            if (track[index].angle<0.0) r -= laneDist;
            else r += laneDist;

            if (speed>(Math.sqrt(Vc*r)+0.8)) throttle = 0.0;
            else throttle = 1.0;

            //console.dir(curve[r].a/curve[r].b);
            //console.dir(speed);
            //if (speed>-1.0*curve[r].a/curve[r].b) throttle = 0.0;
            //else throttle = 1.0;

            console.dir("Velocidad");
            console.dir(speed);
            console.dir(throttle);

            console.dir(Math.sqrt(Vc*r)+0.8);

            send({
              msgType: "throttle",
              data: throttle
            });
          }
        }

        break;
      }
    }

  } else {
    if (data.msgType === 'join') {
      console.log('Joined');
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } 
    else if (data.msgType === 'yourCar') {
      carColor = data.data.color;
    } 
    else if (data.msgType === 'gameInit') {
      track = data.data.race.track.pieces;

      var lanes = data.data.race.track.lanes;

      laneDist = 10.0;
    } 

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});


//node index testserver.helloworldopen.com 8091 bot /DVYNIZIZcWKzg